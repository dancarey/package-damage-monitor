#include <Arduino.h>

// Constants
#define TOUCH_PIN_1 T0
#define TOUCH_THRESHOLD 5 // Touch reading must be below this to trigger an event

// Lists the reason for a wakeup event.
void act_on_wakeup(){

  esp_sleep_wakeup_cause_t wakeup_cause;

  wakeup_cause = esp_sleep_get_wakeup_cause();

  // Here we would kick off an event based on the cause of wakeup
  switch(wakeup_cause)
  {
    case ESP_SLEEP_WAKEUP_EXT0 : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
    default : Serial.printf("Unknown cause for wakeup: %d\n",wakeup_cause); break;
  }
}

// placeholder for now, touch action from sleep will trigger this function
void touchCallback(){  }

// Setup gets run on bootup AND after exiting deep sleep mode.
void setup(){

  // Start serial for debugging
  Serial.begin(115200);
  delay(1000); //delay for serial startup

  // User function which will 
  act_on_wakeup();
  
  // Re-register wakeup event
  touchAttachInterrupt(TOUCH_PIN_1, touchCallback, TOUCH_THRESHOLD);

  //Register the touchpad as a reason to wakeup
  esp_sleep_enable_touchpad_wakeup();

  Serial.println("Entering sleep...");

  // Enter deep sleep
  esp_deep_sleep_start();
}

// loop() is just a placeholder, but required by the compiler.
// Final program will be purely event driven and should never enter this looping state.
// The setup function enters the sleep mode which stops it from reaching here.
// Each time a wakeup event occurs, the setup function run.
void loop(){
}