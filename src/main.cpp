#include <Arduino.h>

#include <Arduino.h>
#include <time.h>

//Filesystem libraries
#include "FS.h"
#include "SPIFFS.h"

// Bluetooth libraries
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

#define SERVICE_UUID "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

/* You only need to format SPIFFS the first time you run a
   test or else use the SPIFFS plugin to create a partition
   https://github.com/me-no-dev/arduino-esp32fs-plugin */
#define FORMAT_SPIFFS_IF_FAILED true

// PIN ASSIGNMENTS
#define TOUCH_PIN_1 T0
#define BUTTON_PIN 14
#define BATTERY_MONITOR_PIN A13

#define BTN_TRANSMIT_MILLIS 1000
#define BTN_CLEAR_MEM_MILLIS 5000
#define TOUCH_THRESHOLD 5

struct TouchData // holds touch event data
{
    unsigned long time;
    unsigned int value;
};

BLEServer *pServer = NULL;
BLECharacteristic *pTxCharacteristic;
bool deviceConnected = false;
bool oldDeviceConnected = false;

// BLE connection status callbacks.
class MyServerCallbacks : public BLEServerCallbacks
{
    void onConnect(BLEServer *pServer)
    {
        deviceConnected = true;
    };

    void onDisconnect(BLEServer *pServer)
    {
        deviceConnected = false;
    }
};

// BLE registered callbacks (use to recieve commands)
class MyCallbacks : public BLECharacteristicCallbacks
{
    void onWrite(BLECharacteristic *pCharacteristic)
    {
        std::string rxValue = pCharacteristic->getValue();

        if (rxValue.length() > 0)
        {
            Serial.println("*********");
            Serial.print("Received Value: ");
            for (int i = 0; i < rxValue.length(); i++)
                Serial.print(rxValue[i]);

            Serial.println();
            Serial.println("*********");
        }
    }
};

// Deletes a file at the given path
void deleteFile(fs::FS &fs, const char *path)
{
    Serial.printf("Deleting file: %s\r\n", path);
    if (fs.remove(path))
    {
        Serial.println("- file deleted");
    }
    else
    {
        Serial.println("- delete failed");
    }
}

// Prints SPIFFS filesystem space status
void printSpaceUsage()
{
    size_t totalBytes = SPIFFS.totalBytes();
    size_t usedBytes = SPIFFS.usedBytes();

    Serial.printf("%u of %u bytes used.\r\n", usedBytes, totalBytes);
}

// Appends the TouchData object data to the file.
void appendTouch(fs::FS &fs, const char *path, TouchData *touch_data)
{

    Serial.printf("Appending touch data to file: %s\r\n", path);

    File file = fs.open(path, FILE_APPEND);
    if (!file || file.isDirectory())
    {
        Serial.println("- failed to open touch file for appending");
        return;
    }

    if (file.write((byte *)touch_data, sizeof(TouchData)))
    {
        Serial.println("- message appended");
    }
    else
    {
        Serial.println("- append failed");
    }
}

// Transmits an array of 20 bytes over BLE
void transmitBytes(char str_buffer[]){

    pTxCharacteristic->setValue((uint8_t *)str_buffer, strlen(str_buffer));
    pTxCharacteristic->notify();
    
    // bluetooth stack will go into congestion if sent to quickly
    delay(3);
}

// ---------------------------------------------------
// transmitTouchData() - pulls data out of the file
// and attempts to transmit it over bluetooth.
// ---------------------------------------------------
void transmitTouchData(fs::FS &fs, const char *path)
{
    Serial.println("Transmitting Touch Data... ");

    File file = fs.open(path);
    if (!file || file.isDirectory())
    {
        Serial.println("- failed to open touch file for reading");
        return;
    }

    TouchData touch_data; //temp to hold data as we extract it from the file.

    char str_buffer[20]; //The notify() function can only transmit 20 bytes at once.

    if (deviceConnected && file.available()) // Send over bluetooth
    {
        // json object start
        // Build a c-style string. snprintf protects against overflow
        snprintf(str_buffer, 20, "{\n \"touches\":[\n");
        transmitBytes(str_buffer);

        //json object key/value pairs
        while (file.available())
        {
            // Read the next touch event from file into the temporary struct
            file.readBytes((char *)&touch_data, sizeof(TouchData));

            // key
            snprintf(str_buffer, 20, " {\"time\":");
            transmitBytes(str_buffer);
            // value
            snprintf(str_buffer, 20, "\"%lu\",", touch_data.time);
            transmitBytes(str_buffer);

            // key
            snprintf(str_buffer, 20, "\n\"value\":");
            transmitBytes(str_buffer);
            // value
            if (file.available()){
                snprintf(str_buffer, 20, "\"%u\"\n},\n", touch_data.value);
            }else{
                snprintf(str_buffer, 20, "\"%u\"\n}\n", touch_data.value);
            }
            transmitBytes(str_buffer);
        }

        snprintf(str_buffer, 20, "]}");
        transmitBytes(str_buffer);
    }
}



// ---------------------------------------------------
// setup() - Runs first, and only runs once.
// ---------------------------------------------------
void setup()
{
    Serial.begin(115200); // Start a serial connection

    // Setup filesystem
    if (!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED))
    {
        Serial.println("SPIFFS Mount Failed");
        return;
    }

    delay(1000);

    printSpaceUsage();

    //deleteFile(SPIFFS, "/touch.dat"); // Clear the data upon power cycle

    // Define pins as inputs or outputs
    pinMode(BUTTON_PIN, INPUT);
    pinMode(BATTERY_MONITOR_PIN, INPUT);

    // Create the BLE Device
    BLEDevice::init("PDM1");

    // Create the BLE Server
    pServer = BLEDevice::createServer();
    pServer->setCallbacks(new MyServerCallbacks());

    // Create the BLE Service
    BLEService *pService = pServer->createService(SERVICE_UUID);

    // Create a BLE Characteristic
    pTxCharacteristic = pService->createCharacteristic(
        CHARACTERISTIC_UUID_TX,
        BLECharacteristic::PROPERTY_NOTIFY);

    pTxCharacteristic->addDescriptor(new BLE2902());

    BLECharacteristic *pRxCharacteristic = pService->createCharacteristic(
        CHARACTERISTIC_UUID_RX,
        BLECharacteristic::PROPERTY_WRITE);

    pRxCharacteristic->setCallbacks(new MyCallbacks());

    // Start the service
    pService->start();

    // Start advertising
    pServer->getAdvertising()->start();
    Serial.println("Waiting a client connection to notify...");

    Serial.println("Setup complete.");
}

// ---------------------------------------------------
// main loop() - Runs after setup(), and runs forever
// ---------------------------------------------------
void loop()
{
    delay(1000); // until wakeup events are implemented, loop every 1 second

    // Get the touch pin value and append it to the file
    TouchData touch_data_in = {millis(), touchRead(TOUCH_PIN_1)};
    Serial.print("Touch Sensor: ");
    Serial.println(touch_data_in.value);

    if(touch_data_in.value < TOUCH_THRESHOLD){
        appendTouch(SPIFFS, "/touch.dat", &touch_data_in);
    }

    // Push Button --------------------------------------
    // read the state of the switch into a local variable:
    int btn_start_millis = millis();
    int mode = 0;

    // If the push button goes high, start counting how long
    // the button is pressed.  If held long enough, set 'mode' accordingly
    while (digitalRead(BUTTON_PIN) == HIGH)
    {
        Serial.println("BTN is HIGH");

        delay(1000);

        if (millis() - btn_start_millis >= BTN_CLEAR_MEM_MILLIS)
        {
            mode = 1;
            Serial.println("BTN press triggered clear mem.");
            deleteFile(SPIFFS, "/touch.dat");
            break;
        }
        else if (millis() - btn_start_millis >= BTN_TRANSMIT_MILLIS)
        {
            mode = 2;
        }
    }

    if (mode == 2) // enter transmit mode
    {
        Serial.println("Transmitting touch data.");
        transmitTouchData(SPIFFS, "/touch.dat");
        printSpaceUsage();
    }
    // END Push Button -----------------------------------


    // disconnecting
    if (!deviceConnected && oldDeviceConnected)
    {
        delay(500); // give the bluetooth stack the chance to get things ready
        pServer->startAdvertising(); // restart advertising
        Serial.println("start advertising");
        oldDeviceConnected = deviceConnected;
    }
    // connecting
    if (deviceConnected && !oldDeviceConnected)
    {
        // do stuff here on connecting
        oldDeviceConnected = deviceConnected;
    }
}