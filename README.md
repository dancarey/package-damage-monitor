# Package Damage Monitor

The Package Damage Monitor (PDM) is a small, lightweight, battery powered monitoring device intended to help detect and record events which cause damage to shipped packages while in-route to their destination.  This project was designed by students at Texas A&M Univeristy as part of their senior design project.

# Project Subsystems

The PDM consists of three main subsystems: The hardware device, a companion mobile app, and a website for saving/viewing gathered data.

# The Hardware

The physical hardware consists of a custom PCB containing the following system components:

- IFExpress ESP32 microcontroller
- BMP 280 environmental sensor
    - Humidity, barometric pressure, and temperature
- ADXL343 3-axis accelerometer
- Light sensor using a photoresistor
- Liquid sensor (custom design)

# The Software

The ESP32 microcontroller software was developed using base Arduino framework, and publicly available sensor drivers.

The mobile application is an iOS app designed using the Swift 5 language.

The webserver components are still under development.
